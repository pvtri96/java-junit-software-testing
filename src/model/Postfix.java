package model;

import java.util.Scanner;
import java.util.Stack;

public class Postfix {

    /**
     * ** Basic **
     * @postfix biểu thức hậu tố
     * @infix   biểu thức trung tố
     * @value   giá trị của biểu thức
     * ** Xử lí  **
     * @firstAlphabetIndex vị trí của toán hạng đầu tiên là Alphabet
     */
    private String postfix, infix;
    private double value;
    private int firstAlphabetIndex = -1;

    /**
     * Biểu thức hậu tố
     * @param  postfix [description]
     * @return         [description]
     */
    public Postfix(String postfix) {
        this.postfix = postfix;
        value = 0;
        if (!check()) {
            this.postfix = "";
        }
    }

    public String getPostfix() {
        return postfix;
    }

    public double getValue() {
        return value;
    }

    public void setPostfix(String postfix) {
        this.postfix = postfix;
    }

    public void setValue(double value) {
        this.value = value;
    }

    /**
     * Lấy phần tử là toán hạng đầu tiên của biểu thức
     * với điều kiện là phần tử đó là Alphabet (Ex: a,b,c...)
     * @return [description]
     */
    public String getFirstAlphabet() {
        for (int i = 0; i < postfix.length(); i++) {
            String temp = Character.toString(postfix.charAt(i));
            if (Check.isAlphabet(temp)) {
                firstAlphabetIndex = i;
                return temp;
            }
        }
        return null;
    }

    /**
     * Set giá trị cho toán hạng Alphabet đầu tiêu của biểu thức
     * @param value Giá trị sẽ được Set
     */
    public void setFirstAlphabet(String value) {
        postfix = postfix.substring(0, firstAlphabetIndex) + value + postfix.substring(firstAlphabetIndex + 1);
    }

    /**
     * Kiểm tra biểu thức hậu tố
     * @return [description]
     */
    public boolean check() {
        if (postfix.equals("")) {
            return false;
        };
        Stack<String> stack = new Stack<String>();
        kb = new Scanner(System.in);
        scan = new Scanner(postfix);
        while (scan.hasNext()) {
            String x = scan.next();
            if (!Check.isOperator(x)) {
                stack.push(x);
            } else {
                if (stack.isEmpty() || !Check.isOperand(stack.pop())) {
                    return false;
                }
                if (stack.isEmpty() || !Check.isOperand(stack.peek())) {
                    return false;
                }
            }
        }
        return stack.size() == 1;
    }

    /**
     * Chuyển sang biểu thức trung tố
     * @return biểu thức trung tố
     * @status chưa hoàn thành
     */
    public String toInfix() {
        //TO-DO here
        return infix;
    }
    private Scanner scan, kb;

    /**
     * Tính giá trị của biểu thức hậu tố
     * @return giá trị 
     */
    public String toValue() {
        if (postfix.equals("")) {
            return "Cant solve.";
        }
        kb = new Scanner(System.in);
        Stack<String> calculate = new Stack<String>();
        scan = new Scanner(postfix);
        while (scan.hasNext()) {
            String x = scan.next();
            if (Check.isNumber(x)) {
                calculate.push(x);
            }
            if (Check.isAlphabet(x)) {
                System.out.print("Enter " + x + ": ");
                calculate.push(kb.nextDouble() + "");
            }
            if (Check.isOperator(x)) {
                double b = Double.parseDouble(calculate.pop());
                double a = Double.parseDouble(calculate.pop());
                double result;
                switch (x) {
                    case "+":
                        result = a + b;
                        break;
                    case "-":
                        result = a - b;
                        break;
                    case "*":
                        result = a * b;
                        break;
                    case "/":
                        result = a / b;
                        break;
                    case "%":
                        result = a % b;
                        break;
                    case "^":
                        result = Math.pow(a, b);
                        break;
                    default:
                        result = 0;
                        break;
                }
                calculate.push(result + "");
            }
            System.out.println(calculate);
        }
        value = Double.parseDouble(calculate.pop());
        return value + "";
    }

    public static void main(String[] args) {
		//InfixToPostfix itp = new InfixToPostfix("5 E ^ D C B ^ A + * + ");
        //Infix itp = new Infix("( x + y ) ^ 2 - 3 * ( z - 2 ) / 4 + h % 2");
        Infix itp = new Infix("27 / ( 3 - a * 12 ) + 9");

        Postfix ptv = new Postfix(itp.toPostfix());
        System.out.println(ptv.postfix);
        System.out.println(ptv.toValue());
    }
}
