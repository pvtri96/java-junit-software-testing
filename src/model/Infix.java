package model;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

public class Infix {

    /**
     * ** Basic **
     * @prefix  biểu thức tiền tố
     * @infix   biểu thức trung tố
     * @posfix  biểu thức hậu tố
     * @value   giá trị của biểu thức
     * ** Xử lí  **
     * @firstAlphabetIndex vị trí của toán hạng đầu tiên là Alphabet
     */
    private String infix, postfix, prefix;
    private double value;
    private int firstAlphabetIndex = -1;

    /**
     * Biểu thức trung tố
     * @param  inFix [description]
     * @return       [description]
     */
    public Infix(String infix) {
        this.infix = infix;
        postfix = "";
        prefix = "";
        if (!check()) {
            this.infix = "";
            postfix = prefix = "Cant solve this type of String.";
        }
    }

    public String getInfix() {
        return infix;
    }

    public String getPostfix() {
        return postfix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setInfix(String infix) {
        this.infix = infix;
    }

    public void setPostfix(String postfix) {
        this.postfix = postfix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * Lấy phần tử là toán hạng đầu tiên của biểu thức
     * với điều kiện là phần tử đó là Alphabet (Ex: a,b,c...)
     * @return [description]
     */
    public String getFirstAlphabet() {
        for (int i = 0; i < infix.length(); i++) {
            String temp = Character.toString(infix.charAt(i));
            if (Check.isAlphabet(temp)) {
                firstAlphabetIndex = i;
                return temp;
            }
        }
        return null;
    }

    /**
     * Set giá trị cho toán hạng Alphabet đầu tiêu của biểu thức
     * @param value Giá trị sẽ được Set
     */
    public void setFirstAlphabet(String value) {
        infix = infix.substring(0, firstAlphabetIndex) + value + infix.substring(firstAlphabetIndex + 1);
    }
    private Scanner scan;

    /**
     * Kiểm tra biểu thức trung tố có hợp lệ hay không
     * @return [description]
     */
    public boolean check() {
        if (infix.equals("")) {
            postfix = prefix = "Cant solve this type of String.";
            return false;
        }
        if (Check.isBrace(infix) == false) {
            return false;
        }
        scan = new Scanner(infix);
        ArrayList<String> check = new ArrayList<String>();
        while (scan.hasNext()) {
            check.add(scan.next());
        }
        if (Check.isOperator(check.get(0))) {
            return false;
        }
        if (Check.isOperator(check.get(check.size() - 1))) {
            return false;
        }
        for (int i = 1; i < check.size() - 1; i++) {
            String a = check.get(i - 1);
            String b = check.get(i + 1);
            if (Check.isOperator(check.get(i))) {
                if ((!Check.isOperand(a) && !Check.isBracket(a)) || (!Check.isOperand(b) && !Check.isBracket(b))) {
                    return false;
                }
            }
            if (Check.isOperand(check.get(i))) {
                if ((!Check.isOperator(a) && !Check.isBracket(a)) || (!Check.isOperator(b) && !Check.isBracket(b))) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Chuyển biểu thức trung tố sang hậu tố
     * @return biểu thức hậu tố
     */
    public String toPostfix() {
        if (!check()) {
            return postfix;
        }
        Stack<String> operatorStack = new Stack<String>();
        scan = new Scanner(infix);
        while (scan.hasNext()) {
            String x = scan.next();
            if (Check.isOperand(x)) {
                postfix += x + " ";
            }
            if (Check.isOpen(x)) {
                operatorStack.push(x);
            }
            if (Check.isOperator(x)) {
                while (!operatorStack.isEmpty()
                        && !Check.isOpen(operatorStack.peek())
                        && Check.priority(x) >= Check.priority(operatorStack.peek())) {
                    postfix += operatorStack.pop() + " ";
                }
                operatorStack.push(x);
            }
            if (Check.isClose(x)) {
                while (!Check.isOpen(operatorStack.peek())) {
                    postfix += operatorStack.pop() + " ";
                }
                operatorStack.pop();
            }
        }
        while (!operatorStack.isEmpty()) {
            postfix += operatorStack.pop() + " ";
        }
        return postfix;
    }

    /**
     * Chuyển biểu thức trung tố sang tiền tố
     * @return biểu thức tiền tố
     */
    public String toPrefix() {
        if (infix.equals("") || !check()) {
            return prefix;
        }
        String infix = Check.reverse(this.infix);
        Stack<String> stack = new Stack<String>();
        scan = new Scanner(infix);
        while (scan.hasNext()) {
            String x = scan.next();
            if (Check.isOperand(x)) {
                prefix += x + " ";
            }
            if (Check.isOperator(x)) {
                while (!stack.isEmpty()
                        && Check.priority(x) >= Check.priority(stack.peek())
                        && !Check.isOpen(stack.peek())) {
                    prefix += stack.pop() + " ";
                }
                stack.push(x);
            }
            if (Check.isOpen(x)) {
                stack.push(x);
            }
            if (Check.isClose(x)) {
                while (!stack.isEmpty() && !Check.isOpen(stack.peek())) {
                    prefix += stack.pop() + " ";
                }
                stack.pop();
            }
        }
        while (!stack.isEmpty()) {
            prefix += stack.pop() + " ";
        }
        prefix = Check.reverse(prefix);
        return prefix;
    }

    /**
     * Tính giá trị của biểu thức trung tố
     * @return giá trị
     * @status chưa hoàn thành
     */
    public double toValue() {
        //TO-DO here
        return value;
    }

    public static void main(String[] args) {
        Infix a;
        a = new Infix("A ^ B * ( C ^ ( D / E ) ^ F ^ G ) - H /K");
        System.out.println(a.toPrefix());
        System.out.println(a.toPostfix());
    }
}
