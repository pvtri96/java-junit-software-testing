package model;

import java.util.Scanner;
import java.util.Stack;

public class Prefix {

    /**
     * ** Basic **
     * @prefix  biểu thức tiền tố
     * @infix   biểu thức trung tố
     * @value   giá trị của biểu thức
     * ** Xử lí  **
     * @firstAlphabetIndex vị trí của toán hạng đầu tiên là Alphabet
     */
    private String prefix, infix;
    private double value;
    private int firstAlphabetIndex = -1;

    /**
     * Biểu thức tiền tố
     * @param  prefix [description]
     * @return        [description]
     */
    public Prefix(String prefix) {
        this.prefix = prefix;
        value = 0;
        if (!check()) {
            this.prefix = "";
            System.out.println("Cant solve this type of String");
        }
    }

    public String getPrefix() {
        return prefix;
    }

    public double getValue() {
        return value;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setValue(double value) {
        this.value = value;
    }

    /**
     * Lấy phần tử là toán hạng đầu tiên của biểu thức
     * với điều kiện là phần tử đó là Alphabet (Ex: a,b,c...)
     * @return [description]
     */
    public String getFirstAlphabet() {
        for (int i = 0; i < prefix.length(); i++) {
            String temp = Character.toString(prefix.charAt(i));
            if (Check.isAlphabet(temp)) {
                firstAlphabetIndex = i;
                return temp;
            }
        }
        return null;
    }

    /**
     * Set giá trị cho toán hạng Alphabet đầu tiêu của biểu thức
     * @param value Giá trị sẽ được Set
     */
    public void setFirstAlphabet(String value) {
        prefix = prefix.substring(0, firstAlphabetIndex) + value + prefix.substring(firstAlphabetIndex + 1);
    }
    /**
     * Kiểm tra biểu thức tiền tố có hợp lệ hay không.
     * Đảo ngược lại chuỗi tiền tố rồi kiểm tra giống như kiểm tra postfix.
     * @return [description]
     */
    public boolean check() {
        String revPrefix = Check.reverse(prefix);
        if (revPrefix.equals("")) {
            return false;
        };
        Stack<String> stack = new Stack<String>();
        scan = new Scanner(revPrefix);
        while (scan.hasNext()) {
            String x = scan.next();
            if (!Check.isOperator(x)) {
                stack.push(x);
            } else {
                if (stack.isEmpty() || !Check.isOperand(stack.pop())) {
                    return false;
                }
                if (stack.isEmpty() || !Check.isOperand(stack.peek())) {
                    return false;
                }
            }
        }
        return stack.size() == 1;
    }

    /**
     * Chuyển sang biểu thức trung tố
     * @return biểu thức trung tố
     * @status chưa hoàn thành
     */
    public String toInfix() {
        //TO-DO here
        return infix;
    }
    private Scanner scan, kb;

    /**
     * Tính giá trị của biểu thức tiền tố
     * @return giá trị 
     */
    public String toValue() {
        if (prefix.equals("")) {
            return "Cant solve.";
        }
        kb = new Scanner(System.in);
        Stack<String> cal = new Stack<String>();
        scan = new Scanner(prefix);
        while (scan.hasNext() || cal.size() > 1) {
            String x = scan.hasNext() ? scan.next() : " ";
            while (Check.isOutermostElementNumber(cal)) {
                double b = Double.parseDouble(cal.pop());
                double a = Double.parseDouble(cal.pop());
                double result;
                switch (cal.pop()) {
                    case "+":
                        result = a + b;
                        break;
                    case "-":
                        result = a - b;
                        break;
                    case "*":
                        result = a * b;
                        break;
                    case "/":
                        result = a / b;
                        break;
                    case "%":
                        result = a % b;
                        break;
                    case "^":
                        result = Math.pow(a, b);
                        break;
                    default:
                        result = 0;
                        break;
                }
                cal.push(result + "");
            }
            if (Check.isAlphabet(x)) {
                System.out.println("Enter " + x + ": ");
                cal.push(kb.next());
            }
            if (Check.isOperator(x) || Check.isNumber(x)) {
                cal.push(x);
            }
            System.out.println(cal);
        }
        value = Double.parseDouble(cal.pop());
        return value + "";
    }

    public static void main(String[] args) {
        Prefix a = new Prefix(" - * ^ A B ^ ^ ^ / D E C F G / H K");
        //Prefix a = new Prefix("- * ^ A B ^ ^ ^ C / D E F G / H K"); //( ^ ^a + 4 ) ^ 2 - x / ( 2 + 5 ) + 9 * 3
        //Prefix a = new Prefix("- * 3 4 7");
        //PrefixToValue a = new PrefixToValue("- + 3 4 * 2 5");
        System.out.println(a.toValue());

    }
}
