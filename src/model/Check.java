package model;

import java.util.Scanner;
import java.util.Stack;

/**
 * Singleton class Check
 * Sử dụng để kiểm tra các điều kiện cần cho việc xử lí trong việc chuyển đổi
 * Created by Phạm Văn Trí
 */
public class Check {
	/**
	 * Lớp tĩnh (Static)
	 * Không thể khởi tạo một đối tượng Check từ bên ngoài
	 * @return [description]
	 */
	private Check(){
		//
	}
	/**
	 * Kiểm tra một String có phải là một con số 
	 * @param  str [description]
	 * @return     [description]
	 */
	public static boolean isNumber(String str){
		try{
			Double.parseDouble(str);
			return true;
		}catch(Exception ex){
			return false;
		}
	}
	/**
	 * Kiểm tra một String có phải là toán hạng
	 * @param  str [description]
	 * @return     [description]
	 */
	public static boolean isOperand(String str){
		char ch = str.charAt(0);
		return isNumber(str) || (str.length()==1 && isAlphabet(ch+""));
	}
	/**
	 * Kiểm tra một String có phải là a,b,c...x,y,z || A,B,C,...X,Y,Z,...
	 * @param  str [description]
	 * @return     [description]
	 */
	public static boolean isAlphabet(String str){
		int t = (int)str.charAt(0);
		return (t >= 65 && t <= 90) || (t>=97 && t<= 122);
	}
	/**
	 * Kiểm tra một String có phải là dấu ngoặc '(' hoặc ')' hay không 
	 * @param  str [description]
	 * @return     [description]
	 */
	public static boolean isBracket(String str){
		return str.equals("(")||str.equals(")");
	}
	public static boolean isOpen(String str){
		return str.equals("(");
	}
	public static boolean isClose(String str){
		return str.equals(")");
	}
	/**
	 * Kiểm tra độ ưu tiên của toán tử trong một String.
	 * Example: '^' = 1	 '*' = 2 '-' = 3
	 * @param str [description]
	 * @return [description]
	 */
	private final static String OPERATOR="^*/%+-";
	private static Scanner scan;
	public static int priority(String str){
		int check = OPERATOR.indexOf(str.charAt(0));
		return check == 0 ? 1 : check < 4 ? 2 : 3;
	}
	/**
	 * Kiem tra một String có phải là toán hạng
	 * @param  str [description]
	 * @return     [description]
	 */
	public static boolean isOperator(String str){
		return OPERATOR.indexOf(str.charAt(0))>-1;	
	}
	/**
	 * Kiểm tra hai phần tử ngoài cùng của một Stack có phải là con số
	 * @param  stack [description]
	 * @return       [description]
	 */
	public static boolean isOutermostElementNumber(Stack<String> stack) {
		if(stack.size()>=2){
			String a = stack.pop();
			String b = stack.pop();
			stack.push(b);stack.push(a);
			return Check.isNumber(a)&&Check.isNumber(b);	
		}
		return false;
	}
	/**
	 * Kiểm tra dấu ngoặc của biểu thức nhập vào có hợp lệ hay không
	 * @param  s [description]
	 * @return   [description]
	 */
	public static boolean isBrace(String s){
		Stack<Character> stack = new Stack<Character>();
		for(int i=0;i<s.length();i++)
			if(isBracket(s.charAt(i)+"")) 			
				if(isOpen(s.charAt(i)+""))  		
					stack.push(s.charAt(i));
				else							
					if(stack.isEmpty() || !isCompleteBracket(stack.peek(), s.charAt(i)))	
							return false;
					else	stack.pop();					
		return stack.isEmpty();
	}
	/**
	 * Kiểm tra 2 đối số có phải là dấu ngoặc '(' và ')'
	 * @param  a [description]
	 * @param  b [description]
	 * @return   [description]
	 */
	private static boolean isCompleteBracket(char a,char b){
		return a=='(' && b==')';
	}
	/**
	 * Đảo String 
	 * @param  a String sẽ được đảo
	 * @return   [description]
	 */
	public static String reverse(String a){
		String s="";
		Stack<String> stack = new Stack<String>();
		scan = new Scanner(a);
		while(scan.hasNext())
			stack.push(scan.next());
		while(!stack.isEmpty()){
			String x = stack.pop();
			if(Check.isBracket(x))
				x=Check.isClose(x) ? "(" : ")";
			s+=x+" ";
		}			
		return s;	
	}
}
