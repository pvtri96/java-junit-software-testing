package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import model.*;
import test.Case;
import test.Cases;

public class TestAll {

	private static List<Case> list;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Cases cases = new Cases();
		cases.readFile();
		list = cases.getList();
	}

	@Test
	public void testInfixToPostfix() throws Exception {
		String errors = "";
		for (int i = 0; i < list.size(); i++) {
			
			Infix infix = new Infix(list.get(i).getInfix());
			String result = infix.toPostfix().trim();
			String expected = list.get(i).getPostfix();
			
			try {
				assertEquals(result, expected);
				errors += list.get(i).getId() + " passed.\n";
			} catch (AssertionError ex) {
				errors += list.get(i).getId() + " " + ex.getLocalizedMessage() + "\n";
			}
			
		}
		Cases.writeFile(errors, "infixToPostfix");
	}
	
	@Test
	public void testInfixToPrefix() throws Exception {
		String errors = "";
		for (int i = 0; i < list.size(); i++) {
			
			Infix infix = new Infix(list.get(i).getInfix());
			String result = infix.toPrefix().trim();
			String expected = list.get(i).getPrefix();
			
			try {
				assertEquals(result, expected);
				errors += list.get(i).getId() + " passed.\n";
			} catch (AssertionError ex) {
				errors += list.get(i).getId() + " " + ex.getLocalizedMessage() + "\n";
			}
			
		}
		Cases.writeFile(errors, "infixToPrefix");
	}

	@Test
	public void testPostfixToValue() throws Exception {
		String errors = "";
		for (int i = 0; i < list.size(); i++) {
			
			Postfix postfix = new Postfix(list.get(i).getPostfix());
			String result = postfix.toValue().trim();
			String expected = list.get(i).getResult();
			
			try {
				assertEquals(result, expected);
				errors += list.get(i).getId() + " passed.\n";
			} catch (AssertionError ex) {
				errors += list.get(i).getId() + " " + ex.getLocalizedMessage() + "\n";
			}
			
		}
		Cases.writeFile(errors, "postfixToValue");
	}

	@Test
	public void testPrefixToValue() throws Exception {
		String errors = "";
		for (int i = 0; i < list.size(); i++) {
			
			Prefix prefix = new Prefix(list.get(i).getPrefix());
			String result = prefix.toValue().trim();
			String expected = list.get(i).getResult();
			
			try {
				assertEquals(result, expected);
				errors += list.get(i).getId() + " passed.\n";
			} catch (AssertionError ex) {
				errors += list.get(i).getId() + " " + ex.getLocalizedMessage() + "\n";
			}
			
		}
		Cases.writeFile(errors, "prefixToValue");
	}
}
