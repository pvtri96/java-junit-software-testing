package test;

public class Case {
	private String id, description, infix, prefix, postfix, result;

	public Case() {
		this.id = "";
		this.description = "";
		this.infix = "";
		this.prefix = "";
		this.postfix = "";
		this.result = "";
	}

	public Case(String str) {
		String[] array = str.split("[|]");
		for (int i = 0; i < array.length;) {
			this.id = array[i++];
			this.description = array[i++].trim();
			this.infix = array[i++].trim();
			this.prefix = array[i++].trim();
			this.postfix = array[i++].trim();
			this.result = array[i++].trim();
		}
	}

	public Case(String id, String description, String infix, String prefix, String postfix, String result) {
		this.id = id;
		this.description = description;
		this.infix = infix;
		this.prefix = prefix;
		this.postfix = postfix;
		this.result = result;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInfix() {
		return infix;
	}

	public void setInfix(String infix) {
		this.infix = infix;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getPostfix() {
		return postfix;
	}

	public void setPostfix(String postfix) {
		this.postfix = postfix;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "TestCaseObject [id=" + id + ", description=" + description + ", infix=" + infix + ", prefix=" + prefix
				+ ", postfix=" + postfix + ", result=" + result + "]";
	}

	public static void main(String[] args) {
		Case abc = new Case("TC01|Simple math|1 + 1| + 1 1 | 1 1 + |2.0|||");
		System.out.println(abc);
	}
}
