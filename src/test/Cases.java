package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Cases {
	private List<Case> list;

	public Cases() {
		list = new ArrayList<Case>();
	}

	public List<Case> getList() {
		return list;
	}

	public void readFile() throws FileNotFoundException {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(new File("files/data.txt"));
		while (scanner.hasNext()) {

			String str = scanner.nextLine();
			str = str.substring(0, str.length() - 3);

			list.add(new Case(str));
		}
	}

	public static void writeFile(String result, String type) throws FileNotFoundException {

		SimpleDateFormat format = new SimpleDateFormat("YYYYMMddhhmmss");
		String date = format.format(new Date());
		PrintStream printstream = new PrintStream(new File("files/test_log/" + date + "_test_" + type + ".txt"));
		printstream.println(result);

	}

	public static void main(String[] args) throws FileNotFoundException, ParseException {

		Cases a = new Cases();
		a.readFile();
		System.out.println(a.getList());

	}
}
